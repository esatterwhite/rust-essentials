struct Pair<T> {
    first: T,
    second: T
}

fn second<T>(pair: Pair<T> ) -> T {
    pair.second
}

fn sqroot(r:f32) -> Result<f32, String>{
    if r < 0.0 {
        return Err("Number cannot be negetive!".to_string());
    }

    Ok(f32::sqrt(r))
}

fn main ( ){
    let magic_pair: Pair<u32> = Pair { first: 7, second: 42 };
    let pair_of_magicians: Pair<&str> = Pair{ first: "Gandolf", second: "Sauron" };
   let a = second( magic_pair );

   println!("{}", a);

   // Generic Options

   let x: Option<i8> = Some(5);
   let pi: Option<f64> = Some(3.141592);
   let none:Option<f64> = None;
   let none2 = None::<f64>;
   let name: Option<&str> = Some("Joyce");

   let m = sqroot( -5.0 );
   match m {
       Ok(sq) => println!("The square root of 42 is {}", sq),
       Err(str) => println!("{}", str)
   }

   println!("the program has ended");
}
