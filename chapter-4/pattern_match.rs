use std::io;

fn main ( ) {
    println!("What's your name?");
    let mut buf = String::new();
    io::stdin().read_line(&mut buf)
               .ok()
               .expect("Failed to read number");

    let input_num: Result<u32,_> = buf.trim().parse();

    let num = match input_num {
        Ok(num) => num,
        Err(_) => 0
    }; 

    println!("{}", num);
    // same as;
    
    if let Ok(val) = input_num {
        println!("matched {:?}", val);
    } else {
        println!("No Match");
    }

    let magical_number: i32 = 42;
    match magical_number {
        1 => println!("unity!"),
        2 | 3 | 4 | 5 | 7 | 11 => println!("Ok, this are primes"),
        num @ 40...42 => println!("{} is contained in this range", num),
        _ => println!("No magic at all")
    }

    let loki = ("Loki", true, 800u32);

    match loki {
        (name, demi, _) if demi => {
            print!("This is a demigod ");
            println!("called {}", name);
        },

        (name, _, _) if name == "Thor" => {
            println!("this i sthor");
        },

        (_, _, pow) if pow <= 1000 => println!("this is a powerless god"),
        _ => println!("this is something... else")
    }
}
