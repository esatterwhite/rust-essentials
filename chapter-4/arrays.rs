

fn main() {
    let aliens = ["Cherfer", "Fynock", "Shirack", "Zuxu"];
    println!("{:?}", aliens);

    let zuxus = ["Zuxu"; 3];
    let mut empty: [i32;0] = [];

    println!("{:?}", empty); // []
    println!("The first alien is {}", aliens[0]); // Cherfer
    println!("The last alient is {}", aliens.iter().last().unwrap()); // Zuxu

    let pa = &aliens;
    println!("Third item via pointer: {}", pa[2]);

    for a in &aliens {
        println!("the next alien is {}", a);
    }
}
