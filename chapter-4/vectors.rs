

fn main( ) {
    let mut numbers: Vec<i32> = Vec::new();
    let mut magic_numbers = vec![7i32, 42, 47, 45, 54];
    let mut ids: Vec<i32> = Vec::with_capacity( 25 );

    // construct a vector from an iterator via range

    let rgvec: Vec<i32> = (0..7).collect();
    println!("Collected the range into: {:?}", rgvec);

    let values = vec![1,2,3];
    for n in values {
        println!("{}", n);
    }

    numbers.push( magic_numbers[1] );
    numbers.push( magic_numbers[4] );
    println!("{:?}", numbers);

    let fifty_four = numbers.pop();
    println!("{:?}", numbers);


    let slc = &magic_numbers[1..4]; // 42,47,45
    println!("{:?}", slc);


    let location = "Middle-Earth";
    let part = &location[7..12];
    println!("{}", part); // Earth;

    let magician = "Merlin";
    let mut chars: Vec<char> = magician.chars().collect();
    let mut bytes: Vec<u8> = magician.bytes().collect();

    chars.sort();

    for c in chars.iter() {
        print!("{} ", c);
    }

    for b in bytes.iter() {
        print!("{} ", b);
    }
}
