struct Score(i32, u8);

struct Kilograms(u32);

struct Player {
    nname: &'static str, // nickname
    health: i32,
    level: u8
}

struct Monster {
    health: u32,
    level: u8
}

fn main ( ) {
    let score1 = Score(72, 2);
    let Score(h, l) = score1; // desctuction

    println!("Health {} - Level {}", h,l);

    let weight = Kilograms(250);
    let Kilograms(kgm) = weight;
    println!("weight is {} kilograms",kgm);


    let mut pl1 = Player { nname:"Dzenan", health: 73, level: 2 };
    println!("Player {} is at level {}", pl1.nname, pl1.level);
    pl1.level =3;
    println!("Player {} is at level {}", pl1.nname, pl1.level);


    let m = Monster { health: 88, level: 8 };
    println!("Monster ( lv {} ) has {} health points", m.level, m.health);
}
