use std::io;
use PlanetaryMonster::MarsMonster;

type species = &'static str;
enum PlanetaryMonster{
    VenusMonster(species, i32),
    MarsMonster(species, i32)
}

enum Compass {
    North, South, East, West
}


fn main ( ) {
    let direction = Compass::West;
    let martan = MarsMonster("Chela", 42);
    

    println!("What's your name?");
    let mut buf = String::new();
    io::stdin().read_line(&mut buf)
               .ok()
               .expect("Failed to read number");

    let input_num: Result<u32,_> = buf.trim().parse();

}
