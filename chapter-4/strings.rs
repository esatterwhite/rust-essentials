fn how_long(s: &str) -> usize { s.len() }


#[warn(unused_variables)]
fn main (){
    let magician1 = "Merlin";
    let mut str1 = String::new();
    //let str2 = String::with_capacity(25);
    let str3 = magician1.to_string(); // convert a slice into a string

    // let sl1 = &str3; // convert a string into a slice

    if &str3 == magician1 {
        println!("we got the same magician alright");
    }

    let c1 = 'q';
    str1.push( c1 );

    println!("{}", str1); // q
    str1.push_str(" Level 1 is finished -");
    println!("{}", str1); // q Level 1  is finised - 
    str1.push_str("Rise up to Level 2");
    println!("{}", str1); // q Level 1 is finished - Rise up to Level 2

    for c in magician1.chars() {
        print!("{} - ", c);
    }

    for word in str1.split(" "){
        print!("{} / ", word);
    }

    let str5 = str1.replace("Level", "Floor");
    println!( "Length of str1: {}", how_long( &str1 ) );
}
