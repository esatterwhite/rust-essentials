
fn is_even( n: i32 ) -> bool {
    n % 2 == 0
}

fn cube( n: i32 ) -> i32 {
    n * n * n
}
fn main ( ){
    let mut rng = 0..1000;
    let fourty_two = rng.find(|n|{
        *n >= 42
    });

    let rng_even = rng.filter(|n|{
        is_even(*n)
    }).collect::<Vec<i32>>();


    let rng_pow3 = (0..1000).filter(|n| is_even(*n) )
                      .map(|n| n * n * n)
                      .take( 4 )
                      .collect::<Vec<i32>>();


    let prod_cube: i32 = (1..6).fold(1, |prod,n| prod * cube(n) );

    println!("{:?}", fourty_two.unwrap());

    println!("{:?}", rng_even);
    println!("{:?}", rng_pow3);
    println!("{:?}", prod_cube);

}
