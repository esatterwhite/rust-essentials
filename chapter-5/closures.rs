
fn again<F: Fn(i32) -> i32>(f: F, s: i32) -> i32 {
    f(f(s))
}

fn main ( ) {
    let par = again( | n | { n * 3 }, 23);
    println!("result: {}", par);
}
