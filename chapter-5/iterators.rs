

fn main ( ) {
    
    let mut rng = 0..7;

    loop { 
        match rng.next() {
            Some(val) => print!("{} - ", val),
            None => break
        }
    }

    let aliens = ["Cherfer", "Fynock", "Shirack", "Zuxu"];

    for alien in aliens.iter().rev() {
        print!("{} - ", alien)
    }
}
