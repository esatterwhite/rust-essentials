
struct Alien {
    health: u32,
    damage: u32
}

impl Alien {
    fn new( mut h: u32, d: u32 ) -> Alien {
        if h > 100 { h = 100 }
        Alien { health: h, damage: d }
    }

    fn warn() -> &'static str {
        "Leave this planet immediately or peris!"
    }

    fn attack(&self){
        println!("I attack! Your health lowers with {} damage points", self.damage);
    }

    fn attack_and_suffer(&mut self, damage:u32){
        self.health -= damage;
    }
}



struct Complex {
    real: f64,
    img: f64
}

impl Complex{
    fn add(&self, c: Complex) -> Complex {
        Complex{real: c.real + self.real, img: c.img + self.img } 
    }

    fn to_string(&self) -> String {
        format!("{} + {}i", self.real, self.img)
    }

    fn times_ten(&mut self) {
        self.real = self.real * 10.0;
        self.img = f64::abs( self.img ) *  10.0;
    }

    fn abs(&self) -> f64 {
        f64::abs( self.real ) + f64::abs( self.img )
    }
}
fn main ( ){
    let mut bork = Alien { health: 100, damage: 5 };
    let mut borked = Alien::new( 150, 5 );

    println!("{}", Alien::warn());
    bork.attack();
    bork.attack_and_suffer( 32 );

    println!("{}", bork.health);

    let c1 = Complex{ real: 4.0, img: 6.0};
    let c2 = Complex{ real: 1.0, img: 4.0};

    println!("{}", c1.add( c2 ).to_string() );
    println!("{}", c1.abs() );
    println!("{}", c1.to_string() );
}
