use std::num;
use std::ops::Add;

trait Monster {
    fn new( health: u32, damage: u32) -> Self;
    fn attack(&self);
    fn noise(&self) -> &'static str;
    fn attacks_with_sound(&self);
}


struct Alien {
    health: u32,
    damage: u32
}

impl Monster for Alien {
    fn new( health: u32, damage: u32) -> Self {
        Alien{ health: health, damage:damage }
    }

    fn attack(&self){
        println!("I have attacked");
    }

    fn noise( &self ) -> &'static str{
        "squish"
    }

    fn attacks_with_sound(&self){
        println!(" this is some sound: {}", self.noise());
    }
}


trait Draw {
    fn draw(&self); 
}

struct S1{
    field: i32
}

struct S2 {
    field: f32
}

impl Draw for S2 {
    fn draw(&self) {
        println!("{}",self.field * 2.0);
    }
}

impl Add for S2 {
    type Output = S2;
    fn add(self, item: S2) -> S2 {
        S2{ field: self.field + item.field } 
    }
}

impl Draw for S1 {
    fn draw(&self) {
        println!("{}",self.field * 2);
    }
}


fn draw_object<T: Draw>(item: T){
   item.draw();
}

fn main ( ) {
    let a = Alien::new(100, 4);
    println!("{}", a.health);

    let s1 = S1{ field: 100 };
    let s2 = S2{field: 200.4 };
    
    draw_object( s1 );
    draw_object( s2 );
}
